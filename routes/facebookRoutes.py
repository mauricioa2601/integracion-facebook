from flask import Blueprint

from controllers.FacebookController import FacebookController
from controllers.FacebookConfigController import FacebookConfigController

# Instanciando Controllers
facebook = FacebookController()
facebookConfig = FacebookConfigController()

# Registrando en Blueprint
facebookBP = Blueprint('facebookBP', __name__)
facebookConfigBP = Blueprint('facebookConfigBP', __name__)


################################################
########## RUTAS GET Y POST FACEBOOK ###########
################################################

# Ruta para validar el webHook en messenger profile
@facebookBP.route('/webhook')
def validateWebhook():
    return facebook.validate()

# Ruta para recibir los mensajes o delivery de facebook
@facebookBP.route('/webhook', methods=['POST'])
def receivedMessage():
    return facebook.receivedMessage()


################################################
######### CRUD CONFIGURACIÓN  FACEBOOK #########
################################################

# Ruta para crear propiedades
@facebookConfigBP.route('/properties', methods=['POST'])
def createProperties():
    return facebookConfig.createProperties()

# Ruta para eliminar Propiedades
@facebookConfigBP.route('/properties', methods=['DELETE'])
def deleteProperties():
    return facebookConfig.deleteProperties()
