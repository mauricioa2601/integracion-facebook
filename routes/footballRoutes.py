from flask import Blueprint

from controllers.FootballController import FootballController
football = FootBallController()

# Registrando Blueprint
footballBP = Blueprint('footballBP', __name__)

@footballBP.route('/list-countries')
def listCountries():
    return football.listCountries()
