import json


def LogInfo(message, data):
    print(message)
    print(json.dumps(data, indent=2))

def LogError(message, data):
    pass

def LogDebug(message, data):
    pass

from colorama import init
from termcolor import colored 
init()
print(colored('hello world','green','on_red'))

